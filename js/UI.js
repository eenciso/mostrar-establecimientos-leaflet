class UI {

    constructor () {
        // Instanciar API y guardarla como propiedad
        this.api = new API();
        // Crar los markers con LayerGroup
        this.markers = new L.layerGroup();
        // Iniciar el mapa
        this.mapa = this.inicializarMapa();
    }

    inicializarMapa () {
         // Inicializar y obtener la propiedad del mapa
         const map = L.map('mapa').setView([19.390519, -99.3739778], 6);
         const enlaceMapa = '<a href="http://openstreetmap.org">OpenStreetMap</a>';
         L.tileLayer(
             'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
             attribution: '&copy; ' + enlaceMapa + ' Contributors',
             maxZoom: 18,
             }).addTo(map);
         return map;
    }

    mostrarEstablecimientos () {
        this.api.obtenerDatos()
            .then(datos => {
                const resultado = datos.respuestaJSON.results;
                // Ejecutar la funcion para mostrar los Pines
                this.mostrarPines(resultado);
            })
    }

    mostrarPines (datos) {
        // Limpiar los pines (markers)
        this.markers.clearLayers();
        // recorrer los establecimientos
        datos.forEach( dato => {
            // Destructuring
            const {latitude, longitude, calle, regular, premium} = dato;
            // Crear popup
            const opcionesPopUp = L.popup()
                .setContent(`
                    <p>Calle: ${calle}</p>
                    <p><b>Regular:</b> ${regular}</p>
                    <p><b>Premium:</b> ${premium}</p>
                `);
            // Agregar Pin
            const marker = new L.marker([
                parseFloat(latitude),
                parseFloat(longitude)
            ]).bindPopup(opcionesPopUp);
            this.markers.addLayer(marker);
        });
        this.markers.addTo(this.mapa);
    }

    // Buscador
    obtenerSugerencias (busqueda) {
        this.api.obtenerDatos()
            .then(datos => {
                // Obtener Datos
                const resultados = datos.respuestaJSON.results;
                // Enviar el JSON y la busqueda para el filtrado
                this.filtrarSugerencias(resultados, busqueda);
            })
    }

    // Filtra las sugerencias con la base al input
    filtrarSugerencias (resultado, busqueda) {
        // Filtrar con .filter
        // Retorna todos los registros que concuerden con las busqueda
        const filtro = resultado.filter(filtro => filtro.calle.indexOf(busqueda) !== -1);
        // Mostrar los pines
        this.mostrarPines(filtro);
    }
}